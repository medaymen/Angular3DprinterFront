import { Injectable, Injector } from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/throw'
import 'rxjs/add/operator/catch';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

   const token =  localStorage.getItem('id_token');
    const headers = new HttpHeaders()
      .set("x-access-token", token);
    headers.set('Access-Control-Allow-Origin', '*');



   if(token){

    let  AuthRequest = request.clone(
       { headers: headers}
     );
     return next.handle(AuthRequest);
   }else {
     return next.handle(request);
   }

  }
}
