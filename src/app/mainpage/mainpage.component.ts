import { Component, OnInit } from '@angular/core';
import {DialogloginComponent} from "../CustomComponents/LoginDialogComponent/Dialoglogin.component";
import {MatDialog} from "@angular/material";
import {DialogRegisterComponent} from "../CustomComponents/RegisterDialogComponent/dialog-register.component";
import {Router} from "@angular/router";
import {AuthService} from "../Services/auth.service";
import { FeedBackService } from '../Services/feed-back.service';


@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css'],
  providers: [AuthService,FeedBackService]
})
export class MainpageComponent implements OnInit {
  private feedbacks = [];
  constructor(public dialog:MatDialog, 
    private  userService:AuthService, 
    private router:Router,
    private feedBackService:FeedBackService) { }

  ngOnInit() {
     if (this.userService.isLoggedIn()) {
        this.router.navigate(['/main/profile']);
      }
      this.feedBackService.GetAllFeedBacks().subscribe(
        (data)=>{
          this.feedbacks = data ; 
        }
      )
    }
     


  openDialog(): void {
    let dialogRef = this.dialog.open(DialogloginComponent ,{
      width: '360px',
      height:'360px'
    });

  }
  openDialogRegister():void{
    let dialogRef = this.dialog.open(DialogRegisterComponent );
  }

}
