import { TestBed, inject } from '@angular/core/testing';

import { My3dmodelService } from './my3dmodel.service';

describe('My3dmodelService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [My3dmodelService]
    });
  });

  it('should be created', inject([My3dmodelService], (service: My3dmodelService) => {
    expect(service).toBeTruthy();
  }));
});
