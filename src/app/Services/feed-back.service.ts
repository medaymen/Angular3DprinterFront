import { Injectable } from '@angular/core';
import { FeedBack } from '../Models/FeedBack';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class FeedBackService {

  constructor(private http:HttpClient) { }

  public AddFeedBack(feedBack:FeedBack){
   return this.http.post<FeedBack>("http://localhost:3000/api/feedBack/add",feedBack); 
  }
  public GetAllFeedBacks():any{
   return this.http.get<FeedBack[]>("http://localhost:3000/feedBacks"); 
  }

}
