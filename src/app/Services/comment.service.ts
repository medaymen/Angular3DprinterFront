import { Injectable } from '@angular/core';
import {Comment} from "../Models/Comment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class CommentService {

  constructor(private http:HttpClient) { }


  public AddComment(comment:Comment,postid:string,userid:string):any{

    return  this.http.post<Comment>("http://localhost:3000/api/comment/"+postid+"/"+userid+"/add",comment);



  }
  public  getComments(idpost:string):any{
     
     return  this.http.get<Comment[]>("http://localhost:3000/api/Comments/"+idpost);


  }
  public  DeleteComment(idcomment:string,idpost:string):any{

    return this.http.delete("http://localhost:3000/api/Comments/"+idcomment+"/delete/"+idpost);


  }
  public  GetCommentById(idcomment:string):any{

    return this.http.get("http://localhost:3000/api/Comments/"+idcomment);


  }
  public  UpdateComment(idcomment:string,comment:Comment):any{

    return this.http.put("http://localhost:3000/api/Comments/"+idcomment+"/update",comment);


  }

}
