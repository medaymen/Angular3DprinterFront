import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import "rxjs/add/operator/map";
import {User} from "../Models/User";
import {UserResponse} from "../interfaces/UserResponse";

@Injectable()

export class UserService{

constructor(private _http:HttpClient){

}


  public Registration(user:User){
     let jsonObject = {username: user.username,
                       email: user.email,
                       password: user.password,
                       firstname: user.firstname,
                       lastname : user.lastname,
                       address: user.address,
                       profilepicUrl: user.profilepicUrl

     };


    return this._http.post("http://localhost:3000/register",jsonObject).map(
      res=> console.log(res)
    );
  }
  public UpdateProfile(user:User,userid:string){



    return this._http.put<UserResponse>("http://localhost:3000/api/updateprofile/"+userid,user);
  }
  public GetUserById(userid:string){

    return this._http.get<UserResponse>("http://localhost:3000/api/user/"+userid);




  }



  public GetCurrentUser():User{
   let user = new User();

   user._id = JSON.parse(localStorage.getItem("currentuser"))._id;
    user.username = JSON.parse(localStorage.getItem("currentuser")).username;
    user.email = JSON.parse(localStorage.getItem("currentuser")).email;
    user.firstname = JSON.parse(localStorage.getItem("currentuser")).firstname;
    user.lastname = JSON.parse(localStorage.getItem("currentuser")).lastname;
    user.profilepicUrl = JSON.parse(localStorage.getItem("currentuser")).profilepicUrl;
    user.address = JSON.parse(localStorage.getItem("currentuser")).address;
   return  user;
  }
}
