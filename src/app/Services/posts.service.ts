import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Post} from "../Models/Post";
import {Observable} from "rxjs/Observable";
import {PostResponse} from "../interfaces/PostResponse";

@Injectable()
export class PostsService {

  constructor(private http:HttpClient) { }

  public AddPost(post:Post,iduser:string){
    return this.http.post("http://localhost:3000/api/post/"+iduser+"/add",post) ; 
  }
  public getAllPosts():Observable<Post[]> {

    return this.http.get<Post[]>('http://localhost:3000/api/posts');
  }
  public  GetPostById(idpost:string){
    return this.http.get<PostResponse>('http://localhost:3000/api/post/'+idpost);
  }
  public  DeletePost(idpost:string):any{
    return this.http.delete('http://localhost:3000/api/post/'+idpost+'/delete');
  }
  public UpdatePost(post:Post,idpost:string){

     return this.http.put('http://localhost:3000/api/post/'+idpost+'/update',post); 

  }

}
