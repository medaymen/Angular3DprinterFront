import { Injectable } from '@angular/core';
import {HttpClient}  from "@angular/common/http";
import  * as moment from 'moment';
import {User} from "../Models/User";
import {UserResponse} from "../interfaces/UserResponse";
import {Router} from "@angular/router";

@Injectable()
export class AuthService {

  constructor(private http: HttpClient,private router:Router) { }
  Authentication(user:User){

    let json ={username: user.username,password: user.password};

    return this.http.post<UserResponse>("http://localhost:3000/authenticate",json)
      .map(res => {

       if(res.success ===false){
          alert(res.message);
          this.router.navigate(['/home']);
        }else {
          this.setSession(res);
          console.log(res);
         this.router.navigate(['/main/profile']);

        }

      });

  }
  private setSession(authResult) {
    const expiresAt = moment().add(authResult.expiresIn,'second');
console.log(authResult.idToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()) );
    localStorage.setItem("currentuser", JSON.stringify(authResult.user));
  }

  logout() {
    localStorage.removeItem("id_token");
    localStorage.removeItem("expires_at");
    localStorage.removeItem("currentuser");
  }

  public isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const expiration = localStorage.getItem("expires_at");
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }
}
