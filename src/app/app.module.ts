import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {routes} from "./app.router";
//web components
import {AppComponent} from './app.component';
import { ParticlesModule } from 'angular-particle';
import { ProfileComponent } from './main/profile/profile.component';
import { MainComponent } from './main/main.component';
import { StoreComponent } from './main/store/store.component';

// dependencies
import { Error404pageComponent } from './error404page/error404page.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from "./interceptor/tokeninterceptor";
import {HttpClientModule} from "@angular/common/http";
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';
import { Ng2FileTypeModule } from 'ng2-file-type';
import { My3DcollectionComponent } from './main/my3-dcollection/my3-dcollection.component';
import { My3dmodeldetailComponent } from './main/my3dmodeldetail/my3dmodeldetail.component';
import { BlogComponent } from './main/blog/blog.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {DateFormatPipe} from "./pipes/DateFormatPipe";
import {DateTimeFormatPipe} from "./pipes/DateTimeFormatPipe";
import { PostdetailsComponent } from './main/postdetails/postdetails.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import {
  MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule,
  MatToolbarModule, MatSelectModule, MatCardModule, MatGridListModule,MatSnackBarModule
} from '@angular/material';
import {MatDialogModule} from '@angular/material/dialog';
import {DialogloginComponent} from "./CustomComponents/LoginDialogComponent/Dialoglogin.component";
import { DialogRegisterComponent } from './CustomComponents/RegisterDialogComponent/dialog-register.component';
import { UploadStldialogComponentComponent } from './CustomComponents/UploadSTLDialogComponent/upload-stldialog-component.component';
import { PostDialogComponent } from './CustomComponents/post-dialog/post-dialog.component';
import { UpdatePostDialogComponentComponent } from './CustomComponents/update-post-dialog-component/update-post-dialog-component.component';
import { UpdateCommentDialogComponent } from './CustomComponents/update-comment-dialog/update-comment-dialog.component';
import { FeedbackSnackBarComponent } from './CustomComponents/feedback-snack-bar/feedback-snack-bar.component';
import { MessageSnackBarComponent } from './CustomComponents/message-snack-bar/message-snack-bar.component';
import { RegisterMyProductComponent } from './register-my-product/register-my-product.component';
import { ClaimComponent } from './main/claim/claim.component';
import { SearchPipe } from './pipes/search.pipe';


@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    MainComponent,
    Error404pageComponent,
    FileSelectDirective,
    StoreComponent,
    My3DcollectionComponent,
    My3dmodeldetailComponent,
    BlogComponent,
    DateFormatPipe,
    DateTimeFormatPipe,
    PostdetailsComponent,
    MainpageComponent,
    DialogloginComponent,
    DialogRegisterComponent,
    UploadStldialogComponentComponent,
    PostDialogComponent,
    UpdatePostDialogComponentComponent,
    UpdateCommentDialogComponent,
    FeedbackSnackBarComponent,
    MessageSnackBarComponent,
    RegisterMyProductComponent,
    ClaimComponent,
    SearchPipe


  ],
  imports: [
    BrowserModule,
    routes,
    FormsModule,
    HttpClientModule,
    ParticlesModule,
    Ng2FileTypeModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatSelectModule,
    MatCardModule,
    MatGridListModule,
    MatSnackBarModule







  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  exports : [MatButtonModule, MatCheckboxModule],
  entryComponents: [
    DialogloginComponent,
    DialogRegisterComponent,
    UploadStldialogComponentComponent,
    PostDialogComponent,
    UpdatePostDialogComponentComponent,
    UpdateCommentDialogComponent,
    FeedbackSnackBarComponent,
    MessageSnackBarComponent]
  ,
  bootstrap: [AppComponent]
})
export class AppModule { }
