import {ModuleWithProviders} from '@angular/core' ;
import {Routes, RouterModule} from '@angular/router';
import {ProfileComponent} from "./main/profile/profile.component";
import {MainComponent} from "./main/main.component";
import {StoreComponent} from "./main/store/store.component";
import {Error404pageComponent} from "./error404page/error404page.component";
import {My3DcollectionComponent} from "./main/my3-dcollection/my3-dcollection.component";
import {My3dmodeldetailComponent} from "./main/my3dmodeldetail/my3dmodeldetail.component";
import {BlogComponent} from "./main/blog/blog.component";
import {PostdetailsComponent} from "./main/postdetails/postdetails.component";
import {MainpageComponent} from "./mainpage/mainpage.component";
import { RegisterMyProductComponent } from './register-my-product/register-my-product.component';
import { ClaimComponent } from './main/claim/claim.component';


export const router: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },

  {
    path:'home',
    component:MainpageComponent
  },
  {
    path:'main',
    component:MainComponent,
    children :[{
      path:'profile',
      component:ProfileComponent
    },
      {
        path:'store',
        component:StoreComponent
      },
      {
        path:'3Dcollection',
        component:My3DcollectionComponent
      },
      {
        path:'models/details/:filename',
        component:My3dmodeldetailComponent
      },
      {
        path: 'blog',
        component: BlogComponent

      },
      {
        path:'post/details/:idpost',
        component:PostdetailsComponent
      },
      {
        path:'Myproduct',
        component:RegisterMyProductComponent
      },
      {
        path:'Myproduct',
        component:RegisterMyProductComponent
      },
      {
        path:'claim',
        component:ClaimComponent
      }
      
    ]

  },
  {
    path:'404',
    component:Error404pageComponent

  },
  { path: '**',
    redirectTo: '404' }
    ];
export const routes: ModuleWithProviders = RouterModule.forRoot(router);
