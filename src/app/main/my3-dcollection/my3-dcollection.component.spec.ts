import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { My3DcollectionComponent } from './my3-dcollection.component';

describe('My3DcollectionComponent', () => {
  let component: My3DcollectionComponent;
  let fixture: ComponentFixture<My3DcollectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ My3DcollectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(My3DcollectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
