import {Component, HostListener, OnInit} from '@angular/core';
import {PostsService} from "../../Services/posts.service";
import {MatDialog} from "@angular/material";
import {PostDialogComponent} from "../../CustomComponents/post-dialog/post-dialog.component";
import {UserService} from "../../Services/User.Services";
import { UpdatePostDialogComponentComponent } from '../../CustomComponents/update-post-dialog-component/update-post-dialog-component.component';
@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
  providers: [PostsService,UserService]
})
export class BlogComponent implements OnInit {


private   posts = [];
private   URL:string = "http://localhost:3000/uploads/";
private currentuserid:string;
  constructor(private  postService:PostsService ,private dialog:MatDialog, private userService:UserService) { }

  ngOnInit() {

    this.currentuserid = this.userService.GetCurrentUser()._id;
    this.postService.getAllPosts().subscribe(
      (data)=> this.posts = data
    );

  }
  @HostListener('document:UPDATE_POSTS_LIST', ['$event','$event.detail.posts'])
someAction(event,posts) {
  this.posts = posts ;
}

  openDialog():void{
    let dialogRef = this.dialog.open(PostDialogComponent, {
      position: {
        top: '80px',
      },
      width: '500px',
      height: '400px'

    });

  }
  delete(idpost:string):void{
    this.postService.DeletePost(idpost).subscribe(
      (data)=>{
        this.posts = data ;

      }
    )
  }

  openDialogUpdate(postid:string): void {
    this.postService.GetPostById(postid).subscribe(
      (data)=>{
        let dialogRef = this.dialog.open(UpdatePostDialogComponentComponent, {
          position: {
            top: '80px',
          },
          width: '500px',
          height: '400px',
          data: { content: data.content , idpost: postid}

        });
    
      }
    )
   
  }
}
