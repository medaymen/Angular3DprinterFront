import {ChangeDetectionStrategy, Component, OnInit, HostListener} from '@angular/core';
import {PostsService} from "../../Services/posts.service";
import {ActivatedRoute} from "@angular/router";
import {Comment} from "../../Models/Comment";
import {UserService} from "../../Services/User.Services";
import {CommentService} from "../../Services/comment.service";
import { UpdateCommentDialogComponent } from '../../CustomComponents/update-comment-dialog/update-comment-dialog.component';
import { MatDialog } from '@angular/material';


@Component({
  selector: 'app-postdetails',
  templateUrl: './postdetails.component.html',
  styleUrls: ['./postdetails.component.css'],
  providers: [PostsService,UserService,CommentService],

})
export class PostdetailsComponent implements OnInit {
 private  profilepicUrl:string;
 private  firstname:string;
 private  lastname:string;
 private content:string;
 private  Date:string;
 private  idpost:string='';
 private  message:string='';
 private  Comments = [] ;
 private currentuserid:string;
  constructor(private postservice:PostsService,private router:ActivatedRoute,private userService:UserService
  , private commentService:CommentService,
    private dialog:MatDialog
  ) { }

  ngOnInit() {
    this.currentuserid = this.userService.GetCurrentUser()._id;
  this.router.params.subscribe(
    (params)=>this.idpost = params['idpost']
  );
     this.postservice.GetPostById(this.idpost).subscribe(
       (data)=> {

         this.profilepicUrl = "http://localhost:3000/uploads/"+data.user.profilepicUrl;
         this.firstname = data.user.firstname;
         this.lastname = data.user.lastname ;
         this.content = data.content;
         this.Date = data.Date;

       }
     );
 this.commentService.getComments(this.idpost).subscribe(
      (data)=>{
        console.log(data);
        this.Comments = data ;
      }
    );

  }
  AddComment():void{
      let comment = new Comment();
      comment.Content = this.message;
      this.commentService.AddComment(comment,this.idpost,this.userService.GetCurrentUser()._id).subscribe(
        (data)=>{
        this.Comments = data ;

        }
      );
      this.message = " ";


  }
  delete(idcomment:string){

    this.commentService.DeleteComment(idcomment,this.idpost).subscribe(
      (data)=>{
      this.Comments = data ; 
      }
    )
  }
  openDialogUpdate(idcomment:string,content:string){

        let dialogRef = this.dialog.open(UpdateCommentDialogComponent, {
          position: {
            top: '80px',
          },
          width: '500px',
          height: '400px',
          data: { content: content , idcomment: idcomment }

        });
    
      }
    
      @HostListener('document:UPDATE_COMMENTS_LIST', ['$event','$event.detail.comments'])
      someAction(event,comments) {
        this.Comments = comments ;
      }

  }


