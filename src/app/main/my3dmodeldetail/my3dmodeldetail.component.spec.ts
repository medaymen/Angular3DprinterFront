import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { My3dmodeldetailComponent } from './my3dmodeldetail.component';

describe('My3dmodeldetailComponent', () => {
  let component: My3dmodeldetailComponent;
  let fixture: ComponentFixture<My3dmodeldetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ My3dmodeldetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(My3dmodeldetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
