import {Component, ViewChild, OnInit, Renderer2, Input} from '@angular/core';
import * as THREE from 'three';
import * as OrbitControls from 'three-orbitcontrols';
import * as THREESTLLoader from  'three-stl-loader' ;
let STLLoader = new THREESTLLoader(THREE) ;// Added THREE
let loader = new STLLoader(); // Removed THREE
import Scene = THREE.Scene;
import PerspectiveCamera = THREE.PerspectiveCamera;
import WebGLRenderer = THREE.WebGLRenderer;
import {ActivatedRoute, Params, Router} from "@angular/router"
import {My3dmodelService} from '../../Services/my3dmodel.service';

@Component({
  selector: 'app-my3dmodeldetail',
  templateUrl: './my3dmodeldetail.component.html',
  styleUrls: ['./my3dmodeldetail.component.css'],
  providers: [My3dmodelService]
})
export class My3dmodeldetailComponent implements OnInit {

  @ViewChild("myCanvas") myCanvas:any;
  @Input()

  private path:string ;
  private scene: Scene;
  private camera: PerspectiveCamera;
  private renderer: WebGLRenderer;
  private controls: any;
  private routerparam:string;
  constructor(private render: Renderer2,private router:ActivatedRoute,private my3DmodelService:My3dmodelService,
       private  Router:Router){

  }
  ngOnInit() {

    this.router.params.subscribe(
      params=> {
        this.path = "http://localhost:3000/api/STLFiles/"+params['filename'];
        this.routerparam = params['filename'];
      }
    );
    //add listener for the resize of the window - will resize the renderer to fit the window
    let global = this.render.listen('window', 'resize', (evt) => {
      this.onWindowResize();
    })
  }
  ngAfterViewInit(): void {
    this.init3D();
  }
  init3D(){
    // renderer
    this.renderer = new THREE.WebGLRenderer({alpha: true, canvas:  this.myCanvas.nativeElement});
    this.renderer.setSize( 600,500 );

    // scene
    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color( 0xFFFFFF );

    // camera
    this.camera = new THREE.PerspectiveCamera( 35, window.innerWidth / window.innerHeight, 0.01, 10000 );
    this.camera.position.set( 113, 111, 113 );
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.scene.add( new THREE.AmbientLight( 0x222222 ) );
    this.scene.add( this.camera ); // required, because we are adding a light as a child of the camera

    // controls
    this.controls = new OrbitControls(this.camera,this.renderer.domElement);

    // lights
    let light = new THREE.PointLight( 0xffffff, 0.8 );
    this.camera.add( light );

    loader.load(this.path, geometry => {
      let material = new THREE.MeshPhongMaterial( { color: 0xBEBEBE } );

      let mesh = new THREE.Mesh( geometry, material );
      this.scene.add(mesh)
    })

    //request animation
    this.animate();

  }

  /**
   * render the scene and request the window animation frame
   */
  animate() {

    this.camera.lookAt( this.scene.position );

    this.renderer.render(this.scene, this.camera);

    window.requestAnimationFrame(_ => this.animate());

  }

  /**
   * will resize the renderer and the camera to the right size of the window
   */
  onWindowResize() {

    this.camera.aspect = window.innerWidth / window.innerHeight;

    this.camera.updateProjectionMatrix();

    this.renderer.setSize( window.innerWidth, window.innerHeight );

  }

   Delete(){
this.my3DmodelService.DeleteModel(this.routerparam).subscribe(
  (data)=>{
       console.log(data);
       this.Router.navigate(['/main/3Dcollection']);
  }
)
   }
}
