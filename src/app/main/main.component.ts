import { Component, OnInit } from '@angular/core';
import {AuthService} from "../Services/auth.service";
import {Router} from "@angular/router";
import { MatSnackBar } from '@angular/material';
import { FeedbackSnackBarComponent } from '../CustomComponents/feedback-snack-bar/feedback-snack-bar.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  providers:[AuthService]
})
export class MainComponent implements OnInit {

  public  tabs = [
    { route :'profile',
      icon :'pe-7s-user',
      tabname :'User Profile',
      isactive:''
     },
    { route :'store',
      icon :'pe-7s-cart',
      tabname :'3D Store',
      isactive:''
    },
    { route :'3Dcollection',
      icon :'pe-7s-star',
      tabname :'My 3D Collection',
      isactive:''
    },
    { route :'blog',
      icon :'pe-7s-medal',
      tabname :'Blog',
      isactive:''
    },
    {
      
      route :'claim',
      icon :'pe-7s-news-paper',
      tabname :'claim',
      isactive:''
    },
    {
      route :'notifications',
      icon :'pe-7s-bell',
      tabname :'Notifications',
      isactive:''
    }

    ];
  public tabactive:string = "";
  myStyle: object = {};
  myParams: object = {};
  width: number = 100;
  height: number = 100;

  constructor( private  authService: AuthService,private router:Router,public snackBar: MatSnackBar) {
    


  }
  logout(){
    this.authService.logout();
    this.router.navigate(['/home']);

  }
  selectTab(item){
     for(let i=0 ; i<this.tabs.length;i++){
       if(i != item && this.tabs[i].isactive=='active'){
         this.tabs[i].isactive = '';
       }else {
         this.tabs[item].isactive= "active";
       }
     }


  }

  ngOnInit() {
  
    this.snackBar.openFromComponent(FeedbackSnackBarComponent);
    this.tabs[0].isactive='active';
    let body = document.getElementsByTagName('body')[0];
    body.setAttribute("style","background-image: url(\"../../assets/images/3af9e2d2628ab1842d2344a9d1d84ea0.jpg\")");
    if(this.authService.isLoggedOut()==true){
      this.router.navigate(['/home']);
      let body = document.getElementsByTagName('body')[0];
      body.setAttribute("style","background:");
    }
    this.myStyle = {
      'position': 'fixed',
      'width': '100%',
      'height': '100%',
      'z-index': -1,
      'top': 300,
      'left': 0,
      'right': 0,
      'bottom': 0,
    }
    this.myParams = {

      particles: {
        number: {
          value: 80,
          density: {
            enable: true,
            value_area: 800
          }
        },
        color: {
          value: '#fffdf9'
        },
        shape: {
          type: "circle",
          polygon: {
            nb_sides: 5
          }
        },
        opacity: {
          value: 0.5,
          random: false,
          anim: {
            enable: false,
            speed: 1,
            opacity_min: 0.1,
            sync: false
          }
        },
        size: {
          value: 5,
          random: true,
          anim: {
            enable: false,
            speed: 40,
            size_min: 0.1,
            sync: false
          }
        },
        line_linked: {
          enable: true,
          distance: 150,
          color: '#e1ffe6',
          opacity: 0.4,
          width: 1
        },
        move: {
          enable: true,
          speed: 6,
          direction: 'none',
          random: false,
          straight: false,
          out_mode: 'out',
          bounce: false,
          attract: {
            enable: false,
            rotateX: 600,
            rotateY: 1200
          }
        }
      },
      interactivity: {
        detect_on: 'canvas',
        events: {
          onhover: {
            enable: true,
            mode: 'grab'
          },
          onclick: {
            enable: true,
            mode: 'push'
          },
          resize: true
        },
        modes: {
          grab: {
            distance: 140,
            line_linked: {
              opacity: 1
            }
          },
          bubble: {
            distance: 400,
            size: 40,
            duration: 2,
            opacity: 8,
            speed: 3
          },
          repulse: {
            distance: 200,
            duration: 0.4
          },
          push: {
            particles_nb: 4
          },
          remove: {
            particles_nb: 2
          }
        }
      },
      retina_detect: true
    };

  }

}
