import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error404page',
  templateUrl: './error404page.component.html',
  styleUrls: ['./error404page.component.css']
})
export class Error404pageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    let body = document.getElementsByTagName('body')[0];
    body.setAttribute("style","background:url('../../assets/images/error-404.png')");
  }

}
