import { Component, OnInit } from '@angular/core';
import {UserService} from "../../Services/User.Services";
import {PostsService} from "../../Services/posts.service";
import {Post} from "../../Models/Post";
import {User} from "../../Models/User";
import {MatDialogRef} from "@angular/material";
import {Router} from "@angular/router";
@Component({
  selector: 'app-post-dialog',
  templateUrl: './post-dialog.component.html',
  styleUrls: ['./post-dialog.component.css'],
  providers:[PostsService,UserService]
})
export class PostDialogComponent implements OnInit {
  private loading:Boolean = false;
  private  content:string="" ;
  constructor(private  postService:PostsService ,
              private userservice:UserService,
              public dialogRef: MatDialogRef<PostDialogComponent>,
              public router:Router
              ) { }

  ngOnInit() {
  }
  post(){
    let user = new User();
    let object = new Post();
    this.loading = true;
    object.content= this.content ;

    this.postService.AddPost(object,this.userservice.GetCurrentUser()._id).subscribe(

      (data)=>{
        this.loading = false;
        this.dialogRef.close();
        let event = new CustomEvent(
          'UPDATE_POSTS_LIST',
          { detail: { 'posts': data } }
        );

        document.dispatchEvent(event);


      }
    );



  }

}
