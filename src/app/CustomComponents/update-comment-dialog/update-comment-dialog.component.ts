import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CommentService } from '../../Services/comment.service';
import { Comment } from '../../Models/Comment';

@Component({
  selector: 'app-update-comment-dialog',
  templateUrl: './update-comment-dialog.component.html',
  styleUrls: ['./update-comment-dialog.component.css'],
  providers : [CommentService]
})
export class UpdateCommentDialogComponent implements OnInit {
  public message:string ; 
  public loading:Boolean= false ; 
  constructor( public dialogRef: MatDialogRef<UpdateCommentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
     public commentService:CommentService){}
  ngOnInit() {
       this.message = this.data.content ; 

  }

  Update(){
  
    this.loading = true ; 
    let comment = new Comment();
     comment.Content = this.message; 
    this.commentService.UpdateComment(this.data.idcomment,comment).subscribe(
    
      (data)=>{
        this.loading = false;
        this.dialogRef.close();
        let event = new CustomEvent(
          'UPDATE_COMMENTS_LIST',
          { detail: { 'comments': data } }
        );

        document.dispatchEvent(event);


      }
    );

    
  
    this.dialogRef.close();

  }

}
