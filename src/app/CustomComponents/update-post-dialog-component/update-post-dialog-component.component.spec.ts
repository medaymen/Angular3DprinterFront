import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePostDialogComponentComponent } from './update-post-dialog-component.component';

describe('UpdatePostDialogComponentComponent', () => {
  let component: UpdatePostDialogComponentComponent;
  let fixture: ComponentFixture<UpdatePostDialogComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePostDialogComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePostDialogComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
