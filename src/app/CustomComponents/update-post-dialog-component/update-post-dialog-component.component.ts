import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PostsService } from '../../Services/posts.service';
import { Post } from '../../Models/Post';

@Component({
  selector: 'app-update-post-dialog-component',
  templateUrl: './update-post-dialog-component.component.html',
  styleUrls: ['./update-post-dialog-component.component.css'],
  providers: [PostsService]
})
export class UpdatePostDialogComponentComponent implements OnInit {
  public message:string ; 
  public loading:Boolean= false ; 
  constructor( public dialogRef: MatDialogRef<UpdatePostDialogComponentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public postService:PostsService) { }

  ngOnInit() {

    this.message = this.data.content ; 

  }

  Update(){
  
    this.loading = true ; 
    let post = new Post();
    post.content = this.message; 
    this.postService.UpdatePost(post,this.data.idpost).subscribe(
    
      (data)=>{
        this.loading = false;
        this.dialogRef.close();
        let event = new CustomEvent(
          'UPDATE_POSTS_LIST',
          { detail: { 'posts': data } }
        );

        document.dispatchEvent(event);


      }
    );

    
  
    this.dialogRef.close();

  }

}



  