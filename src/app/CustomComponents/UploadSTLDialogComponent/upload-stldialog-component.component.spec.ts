import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadStldialogComponentComponent } from './upload-stldialog-component.component';

describe('UploadStldialogComponentComponent', () => {
  let component: UploadStldialogComponentComponent;
  let fixture: ComponentFixture<UploadStldialogComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadStldialogComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadStldialogComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
