import {Component, OnInit} from '@angular/core';
import {My3dmodelService} from "../../Services/my3dmodel.service";
import {FileItem, FileUploader, ParsedResponseHeaders} from "ng2-file-upload";
import {UserService} from "../../Services/User.Services";
import {PostDialogComponent} from "../post-dialog/post-dialog.component";
import {MatDialogRef} from "@angular/material";
import {Router} from "@angular/router";

@Component({
  selector: 'app-upload-stldialog-component',
  templateUrl: './upload-stldialog-component.component.html',
  styleUrls: ['./upload-stldialog-component.component.css'],
  providers: [My3dmodelService, UserService]
})
export class UploadStldialogComponentComponent implements OnInit {
  public uploader: FileUploader;
  public loading: Boolean = false;
  public name: string = '';
  public category: string = '';
  public visibility:string = '';

  constructor(private  my3dmodelservice: My3dmodelService
    , private  userservice: UserService,
              public dialogRef: MatDialogRef<UploadStldialogComponentComponent>
   ) {
  }

  ngOnInit() {
    this.uploader = new FileUploader(
      {
        url: 'http://localhost:3000/api/uploadfile/' + this.userservice.GetCurrentUser()._id,
        headers: [{
          name: 'x-access-token',
          value: localStorage.getItem("id_token")

        }],


      },
    );
    this.uploader.onBeforeUploadItem = (fileItem: any) => {
      this.uploader.options.additionalParameter = {
        imagename: this.name,
        category: this.category, 
        visibility: this.visibility
      };
      alert(this.visibility);
    };
 

  }

  UploadSTL(): void {


    let file = this.uploader.queue[0];
    this.uploader.uploadItem(file);
    this.loading = true;
    this.uploader.onCompleteItem = () => {
      this.loading = false;
      this.dialogRef.close();
      let event = new CustomEvent('UPDATE_MODELS_LIST');

      document.dispatchEvent(event);




    };
    this.uploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);

  }

  onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    let data = JSON.parse(response); //success server response
    console.log(data);
  }

  files: FileList;

  getFiles(event) {
    this.files = event.target.files;
  }

}
