import {Component, OnInit} from "@angular/core";
import {MatDialogRef} from "@angular/material";
import {AuthService} from "../../Services/auth.service";
import {User} from "../../Models/User";
import {Router} from "@angular/router"
@Component({
  selector: 'app-login-dialog',
  styleUrls:['./DialogLogin.component.css'],
  templateUrl: './DialogLoginComponent.html',
  providers: [AuthService]
})
export class DialogloginComponent  implements OnInit{

  constructor(public dialogRef: MatDialogRef<DialogloginComponent>,
              private _userservice:AuthService,
              private router:Router) { }
  myStyle: object = {};
  myParams: object = {};
  width: number = 100;
  height: number = 100;
  public  username:string  ;
  public  password:string ;
  public loading:boolean = false;

  signin(){
    let user = new User();
    user.username = this.username;
    user.password = this.password;
    this.loading = true;
    this._userservice.Authentication(user).subscribe(
      (data)=>{

        this.loading = false
        this.dialogRef.close();
      }
    );
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }
}
