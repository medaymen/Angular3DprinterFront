import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedbackSnackBarComponent } from './feedback-snack-bar.component';

describe('FeedbackSnackBarComponent', () => {
  let component: FeedbackSnackBarComponent;
  let fixture: ComponentFixture<FeedbackSnackBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedbackSnackBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedbackSnackBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
