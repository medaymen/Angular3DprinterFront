import { Component, OnInit, Inject } from '@angular/core';
import { MatSnackBarRef, MatSnackBar } from '@angular/material';
import { UserService } from '../../Services/User.Services';
import { FeedBackService } from '../../Services/feed-back.service';
import { FeedBack } from '../../Models/FeedBack';
import { MessageSnackBarComponent } from '../message-snack-bar/message-snack-bar.component';

@Component({
  selector: 'app-feedback-snack-bar',
  templateUrl: './feedback-snack-bar.component.html',
  styleUrls: ['./feedback-snack-bar.component.css'],
  providers : [UserService,FeedBackService]
})
export class FeedbackSnackBarComponent implements OnInit {
 private message:string; 
  constructor(private matSnackRef:MatSnackBarRef<FeedbackSnackBarComponent>,
              private SnackBar:MatSnackBar,
               private userService:UserService ,
               private feedBackService:FeedBackService) { }

  ngOnInit() {
    
  }
  dissmiss(){

  this.matSnackRef.dismiss();


  }
  AddFeedBack(){
    const user  = this.userService.GetCurrentUser();
    const feedback = new FeedBack();
    feedback.message=this.message ; 
    feedback.user = user ; 
    this.feedBackService.AddFeedBack(feedback).subscribe(
      (data)=>{

        this.SnackBar.openFromComponent(MessageSnackBarComponent,{
          duration: 3000,
          data: { content: "thanks for your feedback !! :)) " }
        });     

      }
    )

  }
}
