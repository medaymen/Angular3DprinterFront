export  interface UserResponse {
  success: boolean;
  message: string;
  idToken: string;
  expiresIn:number
  _id: string;
  username: string;
  email: string;
  firstname: string;
  lastname: string;
  profilepicUrl: string;
  address: string;
}
